import React from "react";
import "./PressPage.css";
import PhoneSvg from "../../assets/phone.svg";
import EMailSvg from "../../assets/mail.svg";
import ImgCard from "../UI/PressImgCard/ImgCard";
import Im1 from "../../assets/office1.png";
import Im2 from "../../assets/blog2.jpg";
import Im3 from "../../assets/blog3.jpg";
import Im4 from "../../assets/blog4.jpg";
import { Button } from "react-bootstrap";
// import { BsArrowRight } from "react-icons/bs";
import Arrow from "../../assets/arrow_icon.png";
import { Link } from "react-router-dom";

const PressPage = () => {
  return (
    <div className="PressPage">
      <div className="image-container">
        <h1>Press</h1>
      </div>
      <div className="contact-us-container">
        <div className="paras">
          <h1>Contact us</h1>
          <p>press@heraf.com</p>
        </div>
        <div className="email-para-phone-container">
          <div className="para">
            <p>
              Our press team loves working with journalists around the world to
              share compelling,
              <br />
              unique stories.If you're a member of the media aand would like to
              talk,please get in
              <br />
              touch with the appropiate team or send an email to{" "}
              <sub>heraf@etsy.com</sub>.
            </p>
            <p>
              Only media inquiries will receive a response.If you're an Heraf
              seller or shopper and
              <br />
              have a question about site,please visit:
              <sub>heraf.com/help</sub>.
            </p>
          </div>
          <div className="email-phone">
            <div className="phone cardcn">
              <div className="contact-type-svg">
                <img src={PhoneSvg} alt="" />
              </div>
              <div className="contact-type-details">
                <h2>Phone Call</h2>
                <p>
                  UAE toll-free phone number <sup>800 686277</sup>
                  <br />
                  We will answer your call <sup>30 - 45 minutes</sup>
                </p>
              </div>
            </div>
            <div className="email cardcn">
              <div className="contact-type-svg">
                <img src={EMailSvg} alt="" />
              </div>
              <div className="contact-type-details">
                <h2>E-mail</h2>
                <p>
                  <span className="contact-emailid">press.uae@heraf.com</span>
                  <br />
                  <span className="contact-emailid">
                    customercare@heraf.com
                  </span>
                  <br />
                  <span className="contact-emailid">support@heraf.com</span>
                  <br />
                  We will reply to you in <sup>12 - 24 hours</sup>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="press-card-container">
        <div className="ready-wraper">
          <div className="container first">
            <div className="cn-card">
              <h1>2021</h1>
              <p>Founded</p>
            </div>
            <div className="cn-card">
              <h1>1,102</h1>
              <p>Employees</p>
            </div>
            <div className="cn-card">
              <h1>
                30<sup>+</sup>m
              </h1>
              <p>Items for sale</p>
            </div>
          </div>
          <div className="container second">
            <div className="cn-card">
              <h1>2.5m</h1>
              <p>Active sellers</p>
            </div>
            <div className="cn-card">
              <h1>
                30<sup>+</sup>m
              </h1>
              <p>Active buyers</p>
            </div>
            <div className="cn-card">
              <h1>$1.2 B</h1>
              <p>Annual gross merchandise sales</p>
            </div>
          </div>
        </div>
      </div>
      <div className="cards-container">
        <div className="heading">
          <h1>What's new at Heraf.</h1>
          <p>From the blog</p>
        </div>
        <div className="cards">
          <div className="container first">
            <div className="row">
              <div className="col">
                <ImgCard
                  image={Im1}
                  title="asdfghjkl"
                  desc="asdfghjklqwertyuiopzxcvbnm,1234567890poiuytrewqasdfghjkl"
                />
              </div>
              <div className="col">
                <ImgCard
                  image={Im2}
                  title="asdfghjkl"
                  desc="asdfghjklqwertyuiopzxcvbnm,1234567890poiuytrewqasdfghjkl"
                />
              </div>
              <div className="col">
                <ImgCard
                  image={Im3}
                  title="asdfghjkl"
                  desc="asdfghjklqwertyuiopzxcvbnm,1234567890poiuytrewqasdfghjkl"
                />
              </div>
            </div>
          </div>
          <div className="container second">
            <div className="row">
              <div className="col">
                <ImgCard
                  image={Im4}
                  title="asdfghjkl"
                  desc="asdfghjklqwertyuiopzxcvbnm,1234567890poiuytrewqasdfghjkl"
                />
              </div>
              <div className="col">
                <ImgCard
                  image={Im2}
                  title="asdfghjkl"
                  desc="asdfghjklqwertyuiopzxcvbnm,1234567890poiuytrewqasdfghjkl"
                />
              </div>
              <div className="col">
                <ImgCard
                  image={Im3}
                  title="asdfghjkl"
                  desc="asdfghjklqwertyuiopzxcvbnm,1234567890poiuytrewqasdfghjkl"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="readmore-btn-container">
        <div className="btn">
          <Button variant="secondary">
            <img src={Arrow} alt="" className="arrow-icon" />
          </Button>
        </div>
        <div className="btn-text">
          <p>Read more on the Heraf news blog</p>
        </div>
      </div>
      <div className="release-container">
        <Link to="" className="link-release">
          Press Releases
        </Link>
      </div>
    </div>
  );
};

export default PressPage;
