import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./HelpPage.css";
import { InputGroup, FormControl, Button, Form } from "react-bootstrap";
import SearchIcon from "../../assets/search.png";
import Arrow from "../../assets/arrow_icon.png";
import Shipping from "../../assets/shipping_help_icon.png";
import Customer from "../../assets/customer_help_icon.png";
import Guranted from "../../assets/guranted_help_icon.png";
import parse from "html-react-parser";

const HelpPage = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [neededText] = useState([
    {
      id: 0,
      heading: "Read our Policies",
      text: "Get the ins and outs of buying and selling on Heraf",
    },
    {
      id: 1,
      heading: "Ask in the forums",
      text: "Learn from our large and knowledgeable community",
    },
    {
      id: 2,
      heading: "See how you're protected",
      text: "Find out more about safety and security in our marketplace",
    },
  ]);
  const [lastContainerData] = useState([
    {
      img: Shipping,
      heading: "Free Shipping",
      text: "On purchases over $399",
    },
    {
      img: Customer,
      heading: "99% satisfied customers",
      text: parse("Our clients opinions speak <br /> for themselves"),
    },
    {
      img: Guranted,
      heading: "Originality Guaranted",
      text: parse("2 years warranty for each product <br /> from our store"),
    },
  ]);

  const submitHandler = (e) => {
    e.preventDefault();
    console.log(searchQuery);

    //Reset Form
    setSearchQuery("");
  };
  return (
    <div className="HelpPage">
      <div className="header">
        <h6>
          By browsing Heraf you agree to our use of{" "}
          <Link to="" className="link">
            cookies
          </Link>
          .
        </h6>
      </div>
      <div className="img-search-container">
        <div className="heading">
          <h1>How can we help?</h1>
        </div>
        <div className="search-input">
          <div className="search">
            <Form onSubmit={submitHandler} className="form">
              <InputGroup>
                <FormControl
                  value={searchQuery}
                  placeholder="Try your question ..."
                  className="control"
                  onChange={(e) => setSearchQuery(e.target.value)}
                />
                <InputGroup.Text>
                  <Button type="submit" className="button">
                    <img src={SearchIcon} alt="" />
                  </Button>
                </InputGroup.Text>
              </InputGroup>
            </Form>
          </div>
        </div>
      </div>
      <div className="third-container">
        <div className="heading">
          <h1>Didn't find what you needed? Try these.</h1>
        </div>
        <div className="btn-container">
          <div className="first-button-container">
            <Button variant="warning">
              Help with on order{" "}
              <img src={Arrow} alt="" className="arrow-icon" />
            </Button>
          </div>
          <div className="second-button-container">
            <div className="btn">
              <Button variant="warning">
                <img src={Arrow} alt="" />
              </Button>
            </div>
            <div className="btn-text">
              <p>Contact Heraf Support</p>
            </div>
          </div>
        </div>
        <div className="text-card-container">
          {neededText?.map((item) => (
            <div className="card-container" key={item.id}>
              <div className="h6">
                <h6>{item.heading}</h6>
              </div>
              <p>{item.text}</p>
            </div>
          ))}
        </div>
      </div>
      <div className="help-card-container">
        <div className="ready-wraper">
          <div className="heading">
            <h1> Featured articles</h1>
          </div>
          <div className="card-text-container">
            <div className="featured-card-container">
              <div className="featured-card">
                <p>Purchasing</p>
                <h3>How to Contact a shop?</h3>
              </div>
              <div className="featured-card">
                <p>Shipping</p>
                <h3>How to Track Your Heraf Order</h3>
              </div>
              <div className="featured-card">
                <p>Purchasing</p>
                <h3>How to Find the Best items for you on Heraf?</h3>
              </div>
            </div>
            <div className="featured-card-container">
              <div className="featured-card">
                <p>Checkout</p>
                <h3>How to Contact Heraf Support</h3>
              </div>
              <div className="featured-card">
                <p>Buying Safely</p>
                <h3>COVID-19 Safety on Heraf</h3>
              </div>
              <div className="featured-card">
                <p>Orders & Returns</p>
                <h3>What's the Status of My Order?</h3>
              </div>
            </div>
            <div className="featured-card-container">
              <div className="featured-card">
                <p>Purchasing</p>
                <h3>How to Search for Items and Shop on Heraf?</h3>
              </div>
              <div className="featured-card">
                <p>Gifting & Favourites</p>
                <h3>How to Buy an Heraf Gift Card</h3>
              </div>
              <div className="featured-card">
                <p>Orders & Returns</p>
                <h3>Change a Wrong Delivery Address on an Order</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="last-container">
        {lastContainerData?.map((item, index) => (
          <div className="last-container-card" key={index}>
            <div className="img-container">
              <img src={item.img} alt="" />
            </div>
            <div className="img-text">
              <h6>{item.heading}</h6>
              <p>{item.text}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default HelpPage;
