import React, { useState } from "react";
import "./UserProfilePage.css";
import CameraIcon from "../../assets/camera-icon.png";
import ProfileIcon from "../../assets/SC.png";
import PenIcon from "../../assets/pen_icon.png";
import HeratIcon from "../../assets/white_heart.png";
import BagIcon from "../../assets/bag-icon.png";
import { Link } from "react-router-dom";
import PlusIcon from "../../assets/plus-icon.png";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import SearchIcon from "../../assets/search.png";
import FilterIcon from "../../assets/filter-icon.png";
import EarthIcon from "../../assets/earth-icon.png";
import DemoProductImg from "../../assets/userProduct.png";
import Card from "../UI/UserProfileProductCard/Card";
import { NavLink } from "react-router-dom";
import UserNewCollectionModal from "../UI/UserNewCollectionModal/UserNewCollectionModal";

const demoProductDetails = [
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
  {
    img: DemoProductImg,
    title: "Jewellery & Accessories Products",
    desc: "The Authentic Corner",
    price: "US$ 50",
    shippingProcess: "Free Shipping",
  },
];

const UserProfilePage = () => {
  const [showModal, setShowModal] = useState(false);
  const showCreateCollectionModal = () => {
    setShowModal(true);
  };
  return (
    <>
      <UserNewCollectionModal
        show={showModal}
        onHide={() => setShowModal(false)}
      />
      <div className="UserProfilePage">
        <div className="profile-container">
          <div className="pf-img">
            <div className="img-container">
              <img src={ProfileIcon} alt="" />
            </div>
            <div className="camera-icon-container">
              <img src={CameraIcon} alt="" />
            </div>
          </div>
          <div className="pf-detail-follower-container">
            <h6>Souvik Chowdhury</h6>
            <p>0 following &#183; 0 followers</p>
          </div>
          <div className="edit-profile-btn-container">
            <button type="button" className="btn btn-secondary">
              <img src={PenIcon} alt="" /> Edit Profile
            </button>
          </div>
        </div>
        <div className="fav-containers">
          <Link to="" className="link">
            <div className="border-container">
              <div className="main-container">
                <img src={HeratIcon} alt="" />
              </div>
            </div>
            <div className="text">
              <p className="first">Favourite items</p>
              <p className="second">0 items</p>
            </div>
          </Link>
          <Link to="" className="link">
            <div className="border-container">
              <div className="main-container">
                <img src={BagIcon} alt="" />
              </div>
            </div>
            <div className="text">
              <p className="first">Favourite Shops</p>
              <p className="second">0 items</p>
            </div>
          </Link>
          <div
            className="add-collection-link"
            onClick={showCreateCollectionModal}
          >
            <div className="add-collection-border-container">
              <div className="add-collection-main-container">
                <img src={PlusIcon} alt="" />
              </div>
            </div>
            <div className="text">
              <p className="first">Create Collections</p>
              <p></p>
            </div>
          </div>
        </div>
        <div className="third-container">
          <div className="search-filter">
            <div className="search-container">
              <div className="search-input">
                <Form className="form">
                  <InputGroup>
                    <FormControl
                      placeholder="all"
                      className="control"
                      required
                    />
                    <InputGroup.Text>
                      <Button type="submit" className="button">
                        <img src={SearchIcon} alt="" /> Search
                      </Button>
                    </InputGroup.Text>
                  </InputGroup>
                </Form>
              </div>
            </div>
            <div className="filter-container">
              <Button type="submit" className="button">
                <img src={FilterIcon} alt="" /> Filters
              </Button>
            </div>
          </div>
          <div className="sort-category-others">
            <div className="sort-categories">
              <div className="select-container">
                <p>Sortby: &nbsp;</p>

                <div className="select-input-container">
                  <Form.Select
                    aria-label="Default select example"
                    className="select-field"
                  >
                    <option value="most-recent">Most Recent</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </Form.Select>
                </div>
              </div>
              <div className="select-container">
                <p>Categories: &nbsp;</p>
                <div className="select-input-container">
                  <Form.Select
                    aria-label="Default select example"
                    className="select-field"
                  >
                    <option value="all">All</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </Form.Select>
                </div>
              </div>
            </div>
            <div className="others">
              <h6>
                <img src={EarthIcon} alt="" /> Public
              </h6>
              <button type="button">
                <img src={PenIcon} alt="" />
              </button>
            </div>
          </div>
        </div>
        <div className="user-profile-product-listing">
          <div className="container">
            <div className="row">
              {demoProductDetails.map((item, index) => (
                <div className="col-lg-3 col-md-6" key={index}>
                  <Card
                    productImg={item.img}
                    title={item.title}
                    desc={item.desc}
                    price={item.price}
                    shippingProcess={item.shippingProcess}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="pagination-container">
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li className="page-item">
                <NavLink to="" className="page-link">
                  Page
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  1
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  2
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  3
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  4
                </NavLink>
              </li>
              <li className="page-item next-link">
                <NavLink to="" className="page-link">
                  Next
                </NavLink>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </>
  );
};

export default UserProfilePage;
