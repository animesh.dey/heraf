import React from "react";
import "./TeamsPage.css";
import { Button } from "react-bootstrap";
// import { BsArrowRight } from "react-icons/bs";
import Arrow from "../../assets/arrow_icon.png";
import LshipCard from "../UI/LeaderShipCard/LshipCard";
import L1 from "../../assets/leader1.png";
import L2 from "../../assets/leader2.png";
import L3 from "../../assets/leader3.png";

const TeamsPage = () => {
  return (
    <>
      <div className="TeamsPage">
        <div className="image-container">
          <h1>Our Team</h1>
        </div>
        {/*2nd container */}
        <div className="second-container">
          <div className="heading">
            <h1>
              The people who work at Heraf <br /> share the vision and values of
              our community.
            </h1>
          </div>
          <div className="para">
            <p>
              We're driven by the idea that the best work is born from
              diligence,
              <br />
              craftsmanship and fun.
            </p>
          </div>
        </div>
        <div className="leadership-card-container">
          <div className="ready-wraper">
            <div className="leadership-heading">
              <h1>Leadership</h1>
            </div>
            <div className="leadership-cards">
              <LshipCard
                image={L1}
                span="Faris A."
                p="Chief Executive Officer"
              />
              <LshipCard
                image={L2}
                span="Kruti Patel Goyal"
                p="Chief Product Officer"
              />
              <LshipCard
                image={L3}
                span="Ryan Scott"
                p="Chief Marketing Officer"
              />
            </div>
            <div className="readmore-btn-container">
              <div className="btn">
                <Button variant="secondary">
                  <img src={Arrow} alt="" className="arrow-icon" />
                </Button>
              </div>
              <div className="btn-text">
                <p>Read more on the team</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TeamsPage;
