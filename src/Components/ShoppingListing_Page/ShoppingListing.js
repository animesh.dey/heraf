import React, { useState } from "react";
import "./ShoppingListing.css";
import {
  Button,
  Form,
  InputGroup,
  FormControl,
  NavLink,
} from "react-bootstrap";
import SearchIcon from "../../assets/search.png";
import ProductCard from "../UI/ProductCard/ProductCard";
import SampleProductPic from "../../assets/shop.jpg";

const demoShoppingDetail = [
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
  {
    img: SampleProductPic,
    title: "The authentic corner",
    desc: "Modern Accessories for Men & Women...",
    items: "288",
  },
];

const ShoppingListing = () => {
  const [searchQuery, setSearchQuery] = useState("");

  const searchSubmitHandler = (e) => {
    e.preventDefault();
    console.log(searchQuery);

    //RESET FORM
    setSearchQuery("");
  };
  return (
    <div className="ShoppingListing">
      <div className="first-container">
        <h1>
          23967 results <span>for loop shop names containing</span>
        </h1>
      </div>
      <div className="second-container">
        <div className="search-input-container">
          <Form className="form" onSubmit={searchSubmitHandler}>
            <InputGroup>
              <FormControl
                value={searchQuery}
                placeholder="all"
                className="control"
                onChange={(e) => setSearchQuery(e.target.value)}
                required
              />
              <InputGroup.Text>
                <Button type="submit" className="button">
                  <img src={SearchIcon} alt="" />
                </Button>
              </InputGroup.Text>
            </InputGroup>
          </Form>
        </div>
        <div className="sort-container">
          <p>Sortby: &nbsp;</p>
          <div className="select-input-field">
            <Form.Select
              aria-label="Default select example"
              className="select-field"
            >
              <option value="relevancy">Relevancy</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
          </div>
        </div>
      </div>
      <div className="container third-container">
        <div className="row row-cols-3">
          {demoShoppingDetail.map((items, index) => (
            <div className="col" key={index}>
              <ProductCard
                img={items.img}
                title={items.title}
                desc={items.desc}
                items={items.items}
              />
            </div>
          ))}
        </div>
      </div>
      <div className="pagination-container">
        <nav aria-label="Page navigation example">
          <ul className="pagination">
            <li className="page-item">
              <NavLink to="" className="page-link">
                Page
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                1
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                2
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                3
              </NavLink>
            </li>
            <li className="page-item next-link">
              <NavLink to="" className="page-link">
                Next
              </NavLink>
            </li>
          </ul>
        </nav>
        <p>Looking for items?Find items containing out</p>
      </div>
    </div>
  );
};

export default ShoppingListing;
