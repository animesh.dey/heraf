import React from "react";
import "./OrderHistoryPage.css";
import {
  Form,
  InputGroup,
  FormControl,
  Button,
  Navbar,
  Container,
  Nav,
} from "react-bootstrap";

import SearchIcon from "../../assets/search.png";
import { NavLink } from "react-router-dom";
import OrderHistoryCard from "../UI/OrderHistoryCard/OrderHistoryCard";

const OrderHistoryPage = () => {
  return (
    <div className="OrderHistoryPage">
      <div className="heading-container">
        <div className="h1">
          <h1>Purchases</h1>
        </div>
        <div className="purchases-settings-prototypes">
          <p>
            <span>Purchases</span>
            <br />
            Cases
          </p>
          <p>
            Settings
            <br />
            Public profile
          </p>
          <p>
            Prototypes
            <br />
            Apps
          </p>
        </div>
      </div>
      <div className="nav-search-input-container">
        <div className="nav-container">
          <Navbar>
            <Container>
              <Nav className="me-auto main-nav">
                <Nav.Link as={NavLink} to="/">
                  All Purchases
                </Nav.Link>
                <Nav.Link as={NavLink} to="/">
                  Not Shipped(2)
                </Nav.Link>
                <Nav.Link as={NavLink} to="/">
                  Canceled(3)
                </Nav.Link>
              </Nav>
            </Container>
          </Navbar>
        </div>
        <div className="search-container">
          <div className="search-input">
            <Form className="form">
              <InputGroup>
                <FormControl
                  placeholder="Search your purchases"
                  className="control"
                  required
                />
                <InputGroup.Text>
                  <Button type="submit" className="button">
                    <img src={SearchIcon} alt="" />
                  </Button>
                </InputGroup.Text>
              </InputGroup>
            </Form>
          </div>
        </div>
      </div>
      <div className="order-history-container">
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
        <OrderHistoryCard />
      </div>
    </div>
  );
};

export default OrderHistoryPage;
