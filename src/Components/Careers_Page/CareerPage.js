import React from "react";
import "./CareerPage.css";
import { Button, Form } from "react-bootstrap";
// import { BsArrowRight } from "react-icons/bs";
import Arrow from "../../assets/arrow_icon.png";
import Wealth from "../../assets/wealth.svg";
import Family from "../../assets/family.svg";
import Community from "../../assets/community.svg";
import { Link } from "react-router-dom";
import Card1 from "../UI/ImageCard1/Card1";
import of1 from "../../assets/office1.png";
import of2 from "../../assets/office2.png";
import of3 from "../../assets/office3.png";

const CareerPage = () => {
  return (
    <div className="CareerPage">
      <div className="img-button-container">
        <div className="img-title">
          <h1>
            Learn. Share. Create. <br /> Build something that matters.
          </h1>
        </div>
        <div className="input-button-img-container">
          <Form className="form">
            <div className="form-input">
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Control
                  type="text"
                  placeholder="Roles"
                  className="control"
                  required
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Control
                  type="text"
                  placeholder="Location"
                  className="control"
                  required
                />
              </Form.Group>
            </div>
            <div className="form-button">
              <Button type="submit">
                Search <img src={Arrow} alt="" className="arrow-icon" />
              </Button>
            </div>
          </Form>
        </div>
      </div>
      {/*second white container */}
      <div className="second-container">
        <div className="timetalk-para-heading">
          <h1>Time to talk benefits</h1>
          <p>
            We believe the best work cames from happy Employees.Here's just a
            few of the professional <br /> and personal perks you'll experience
            at Heraf.
          </p>
        </div>
        <div className="wealth-family-community-container">
          <div className="details">
            <div className="img wealth">
              <img src={Wealth} alt="" />
            </div>
            <div className="title">
              <h3>Wealth</h3>
            </div>
            <div className="para">
              <p>
                Equity (offers vary by country) <br />
                401k matching/pension contributions <br />
                Competitive salary performance bonuses.
              </p>
            </div>
          </div>
          <div className="details">
            <div className="img family">
              <img src={Family} alt="" />
            </div>
            <div className="title">
              <h3>Family</h3>
            </div>
            <div className="para">
              <p>
                Paid gender neutral parental leave <br />
                Adoption and family planning <br />
                Back-up care.
              </p>
            </div>
          </div>
          <div className="details">
            <div className="img community">
              <img src={Community} alt="" />
            </div>
            <div className="title">
              <h3>Community</h3>
            </div>
            <div className="para">
              <p>
                Volunteer time off Donation matching <br />
                Employee resource groups Global <br />
                virtual events.
              </p>
            </div>
          </div>
        </div>
      </div>
      {/*Third Container*/}
      <div className="ready-impact-container">
        <div className="ready-wraper">
          <div className="heading">
            <h1>Ready To Make An Impact? Explore These Open Roles</h1>
          </div>
          <div className="buttons-container">
            <div className="container">
              <div className="row">
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Finance
                  </Link>
                </div>
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Legal
                  </Link>
                </div>
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Marketing & Comms
                  </Link>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Design
                  </Link>
                </div>
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Product & Design
                  </Link>
                </div>
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Research
                  </Link>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Trust & Safety
                  </Link>
                </div>
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Member Services
                  </Link>
                </div>
                <div className="col">
                  <Link to="" className="hyperlink-button">
                    Strategy & Operations
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="hr-text-container">
            <div className="upper-hr">
              <Link to="" className="a">
                Career Site Cookie Settings
              </Link>
              <Link to="" className="a">
                Personal Information
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/*4th container*/}
      <div className="explore-office-container">
        <div className="heading">
          <h1>Explore our offices.</h1>
        </div>
        <div className="para-container">
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum iusto
            expedita natus incidunt ut consequatur a officiis debitis labore
          </p>
          <br />
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Similique
            beatae enim molestias fuga aut quod blanditiis ad, natus porro minus
            hic nam architecto? Reprehenderit suscipit molestias non et enim
            dolor. Lorem ipsum dolor sit, amet consectetur adipisicing elit.
            Facilis eligendi, nulla eos animi sapiente qui, ullam quia officiis
            exercitationem, at ea. Deleniti amet aspernatur labore asperiores
            ullam ipsam veniam qui?
          </p>
          <br />
          <p>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi,
            culpa? Amet recusandae aspernatur eveniet <span>doloribus</span>{" "}
            nihil natus voluptatum recusandae a. Lorem ipsum, dolor sit amet
            consectetur adipisicing elit. Saepe enim harum pariatur omnis, et
            quaerat numquam mollitia optio sed consectetur laudantium doloribus
            at reprehenderit. Iusto delectus corrupti incidunt accusamus
            exercitationem!
          </p>
          <br />
          <p>
            Learn more about our flexible work modes and vaccination policy{" "}
            <span>here.</span>
          </p>
        </div>
      </div>
      <div className="office-imagecard-container">
        <Card1 img={of1} />
        <Card1 img={of2} />
        <Card1 img={of3} />
      </div>
    </div>
  );
};

export default CareerPage;
