import React from "react";
import { Button } from "react-bootstrap";
import Arrow from "../../assets/arrow_icon.png";
import "./AboutPage.css";

const AboutPage = () => {
  return (
    <div className="AboutPage">
      <div className="img-button-container">
        <div className="img-title">
          <h1>
            Find things you'll love. <br /> Only on Heraf.
          </h1>
        </div>
        <div className="button-img">
          <Button>
            Explore Now <img src={Arrow} alt="" className="bs-arrow-right" />
          </Button>
        </div>
      </div>
      <div className="homeofluxury-container">
        <div className="homeofluxury-container-heading">
          <h1>Welcome to the Definitive Home of Luxury</h1>
        </div>
        <div className="homeofluxury-container-paragraphs">
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta
            tempore perferendis sapiente illum fugit repellat provident maxime
            ratione repudiandae natus, officia a quisquam, et nemo quod dolores.
            Maiores, eaque modi! Lorem ipsum dolor sit amet consectetur
            adipisicing elit. Libero veritatis consequuntur sequi ipsa odit
            incidunt officiis sunt. Iste facere et, dolores itaque minima
            perferendis ea, unde, libero quia aliquid sequi.
          </p>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eaque,
            quia. Cupiditate eaque accusamus quisquam porro ex ea facilis
            distinctio atque hic, quaerat voluptate fugiat aspernatur culpa
            velit deleniti corporis voluptatem. Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Facere quasi aliquam vero tenetur
            autem illo, expedita ipsum, ex vitae nam error pariatur laborum sed?
            Aliquam, suscipit cum! Libero, dolore quibusdam.
          </p>
          <p>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint
            possimus neque reprehenderit voluptas quod! Quos nesciunt esse
            deserunt ex saepe. Exercitationem maiores magni alias dolorem
            doloribus ad saepe minus assumenda! Lorem ipsum dolor sit amet
            consectetur adipisicing elit. Similique reiciendis cupiditate
            blanditiis veniam accusamus velit. Optio in, molestiae laborum
            tempora, nobis animi esse minus ea voluptatum voluptas, est
            blanditiis ex.
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis
            voluptate, libero inventore quod excepturi odio quaerat, aliquid
            optio quia animi esse magnam provident repudiandae vero facilis
            ratione non dolor deleniti? Lorem ipsum, dolor sit amet consectetur
            adipisicing elit. Fugiat totam doloremque ducimus, ipsam deleniti
            omnis minima repellat quas est qui minus blanditiis error aliquam
            quia, nesciunt doloribus neque, in esse?
          </p>
        </div>
      </div>
    </div>
  );
};

export default AboutPage;
