import React from "react";
import RedHeartIcon from "../../../assets/red_heart_icon.png";
import { Card } from "react-bootstrap";
import "./Card.css";
import { Link } from "react-router-dom";

const UserCard = (props) => {
  return (
    <Link to="" className="userpf-link">
      <Card
        className="userpf-Card"
        style={{ border: "hidden", outline: "none" }}
      >
        <div className="img-fav-container">
          <div className="img">
            <img src={props.productImg} alt="" />
          </div>
          <div className="fav">
            <img src={RedHeartIcon} alt="" />
          </div>
        </div>
        <div className="product-details">
          <h5>{props.title}</h5>
          <p className="desc">{props.desc}</p>
          <h6>
            {props.price} <span>({props.shippingProcess})</span>
          </h6>
        </div>
      </Card>
    </Link>
  );
};

export default UserCard;
