import React from "react";
import { Link } from "react-router-dom";
import "./ReviewDetailCrd.css";

const ReviewDetailCard = (props) => {
  return (
    <div className="ReviewDetailCard">
      <div className="profile-container">
        <img src={props.profileIcon} alt="" />
      </div>
      <div className="review-details-container">
        <div className="upper">
          <h6>
            <span>{props.profileName}</span> {props.reviewDate}
          </h6>
          <p>
            <img src={props.FullStar} alt="" />
            <img src={props.FullStar} alt="" />
            <img src={props.FullStar} alt="" />
            <img src={props.FullStar} alt="" />
            <img src={props.HalfStar} alt="" />
          </p>
        </div>
        <div className="lower">
          <div className="left">
            <h6>FINSISH:{props.finish}</h6>
            <h6>LENGTH:{props.length}</h6>
            <p>{props.review}</p>
            <Link to="" className="helpful-link">
              Helpful?
            </Link>
          </div>
          <div className="right">
            <img src={props.reviewImg} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReviewDetailCard;
