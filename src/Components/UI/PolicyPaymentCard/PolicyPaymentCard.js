import React from "react";
import "./PolicyPaymentCard.css";

const PolicyPaymentCard = (props) => {
  return (
    <div className="PolicyPaymentCard">
      <img
        className="image"
        src={props.img}
        alt=""
        width={props.width}
        height="auto"
      />
    </div>
  );
};

export default PolicyPaymentCard;
