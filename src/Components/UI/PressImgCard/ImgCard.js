import React from "react";
import "./ImgCard.css";
import { Card } from "react-bootstrap";

const ImgCard = (props) => {
  return (
    <>
      <Card
        style={{
          border: "hidden",
          backgroundColor: "transparent",
          outline: "none",
        }}
        className="pressimgcard"
      >
        <Card.Img variant="top" src={props.image} />
        <Card.Body className="presscardbody">
          <Card.Title className="press-card-title">{props.title}</Card.Title>
          <Card.Text className="press-card-desc">{props.desc}</Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};

export default ImgCard;
