import React from "react";
import { Card } from "react-bootstrap";
import "./Card1.css";

const Card1 = (props) => {
  return (
    <div className="image-card-office">
      <Card className="Card">
        <Card.Img src={props.img} alt="Card image" className="Card-Img" />
      </Card>
    </div>
  );
};

export default Card1;
