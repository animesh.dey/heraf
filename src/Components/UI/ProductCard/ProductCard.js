import React from "react";
import "./ProductCard.css";
import { Card } from "react-bootstrap";
import ShopLogo from "../../../assets/shop-logo.jpg";
import { Link } from "react-router-dom";

const ProductCard = (props) => {
  return (
    <Link to="" className="ProductCard">
      <Card
        style={{ border: "hidden", outline: "none" }}
        className="productCard"
      >
          <Card.Img variant="top" src={props.img} />
        <div className="body">
          <div className="icon">
            <img src={ShopLogo} alt="" />
          </div>
          <div className="title-desc">
            <h6>{props.title}</h6>
            <p>{props.desc}</p>
          </div>
          <div className="quan-items">
            <p>{props.items} items</p>
          </div>
        </div>
      </Card>
    </Link>
  );
};

export default ProductCard;
