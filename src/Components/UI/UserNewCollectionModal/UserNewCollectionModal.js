import React from "react";
import "./UserNewCollectionModal.css";
import { Form, Modal, Button } from "react-bootstrap";
import CloseIcon from "../../../assets/close.png";
import ArrowIcon from "../../../assets/arrow_icon.png";

const UserNewCollectionModal = (props) => {
  const onHideHandler = () => {
    props.onHide();
  };
  const cancelButtonHandler = () => {
    props.onHide();
  };
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.onHide}
        style={{ border: "none", outline: "none" }}
        dialogClassName="UserNewCollectionModal"
      >
        <div className="UserNewCollectionModal-body">
          <div className="closeIcon-container">
            <img src={CloseIcon} alt="" onClick={onHideHandler} />
          </div>
          <div className="heading">
            <h1>Create new Collection</h1>
          </div>
          <div className="textarea-container">
            <Form className="form">
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlTextarea1"
              >
                <Form.Label className="label">Example textarea</Form.Label>
                <Form.Control
                  placeholder="Holiday gift list, Artwork etc…"
                  as="textarea"
                  rows={3}
                  className="control"
                  required
                />
              </Form.Group>
              <div className="switch-container">
                <p>Make this collection private?</p>
                <div className="h4">
                  <p>Yes</p>{" "}
                  <Form.Check
                    type="switch"
                    id="custom-switch"
                    className="switch"
                    required
                  />
                </div>
              </div>
              <div className="content-container">
                <p>
                  If you don’t want others to see this collection, make it
                  private. See our Privacy Policy.
                </p>
              </div>
              <div className="button-container">
                <Button className="cancel-button" onClick={cancelButtonHandler}>
                  Cancel
                </Button>
                <Button className="create-button">
                  Create Collection <img src={ArrowIcon} alt="" />
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default UserNewCollectionModal;
