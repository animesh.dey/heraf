import React from "react";
import { Card } from "react-bootstrap";
import "./LshipCard.css";

const LshipCard = (props) => {
  return (
    <>
      <Card
        bg="transparent"
        className="Lcard"
        style={{ border: "hidden", width: "20rem", textAlign: "center" }}
      >
        <Card.Img variant="top" src={props.image} />
        <Card.Body className="Lbody">
          <p>
            <span>{props.span}</span>
            <br />
            {props.p}
          </p>
        </Card.Body>
      </Card>
    </>
  );
};

export default LshipCard;
