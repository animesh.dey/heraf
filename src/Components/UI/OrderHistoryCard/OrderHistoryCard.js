import React, { useState } from "react";
import "./OrderHistoryCard.css";
import FullStar from "../../../assets/fullstar.png";
import HalfStar from "../../../assets/halfstar.png";
import RightArrow from "../../../assets/right-arrow.png";
import DemoProductImg from "../../../assets/shop-logo.jpg";
import Arrow from "../../../assets/arrow_icon.png";
import ReceiptModal from "../ViewReceiptModal/ReceiptModal";

const OrderHistoryCard = () => {
  const [showReceipt, setShowReceipt] = useState(false);
  const showModalHandler = () => {
    setShowReceipt(true);
  };
  return (
    <>
      <ReceiptModal show={showReceipt} onHide={() => setShowReceipt(false)} />
      <div className="OrderHistoryCard">
        <div className="ordered-product">
          <div className="heading">
            <p>
              Purchased from <sub>The Authentic Corner</sub> on Jul 17,2021
              <br />
              This item was part of a USD 195.99 purchases from multiple shops
            </p>
            <h6>USD 250.00</h6>
          </div>
          <div className="ordered-product-details">
            <div className="product-img">
              <img src={DemoProductImg} alt="" />
            </div>
            <div className="details">
              <h5>
                Embroidered fjallraven kanken backpack , Customized <br />{" "}
                Embroidered Name Backpack
              </h5>
              <p className="first-p">
                Blade& #39;s Design and Leather Variations: Rare Viking Axe
              </p>
              <p className="second-p">
                Personalization: Not requested on this item.
              </p>
              <div className="review-container">
                <div className="your-review-container rc">
                  <div className="review-text-container">
                    <p>Your Review</p>
                  </div>
                  <div className="review-star-container">
                    <img src={FullStar} alt="" />
                    <img src={FullStar} alt="" />
                    <img src={FullStar} alt="" />
                    <img src={FullStar} alt="" />
                    <img src={HalfStar} alt="" />
                  </div>
                </div>
                <div className="edit-review-container rc">
                  <div className="review-text-container">
                    <p>Edit Review</p>
                  </div>
                  <div className="review-star-container">
                    <img src={RightArrow} alt="" className="rightArrow" />
                  </div>
                </div>
              </div>
              <div className="buy-again-button-container">
                <button type="button" className="btn btn-secondary">
                  Buy this again <img src={Arrow} alt="" />
                </button>
                <p>USD 79.50 USD 231.00</p>
              </div>
            </div>
          </div>
        </div>
        <div className="ordered-delivered-container">
          <div className="deliverd-heading-details">
            <h5>Delivered</h5>
            <p>on Aug 8,2021</p>
            <p>
              From saskatoon,SK TO <sub>Ad Diriyah</sub>
              <br />
              Estimated Delivery:Jul 29 Aug,2021
            </p>
          </div>

          <div className="button-container">
            <button type="button" className="btn btn-secondary track-btn">
              Track Package
            </button>
            <button
              type="button"
              className="btn btn-secondary help-order-view-btn"
            >
              Help With Order
            </button>
            <button
              type="button"
              className="btn btn-secondary help-order-view-btn"
              onClick={showModalHandler}
            >
              View Receipt
            </button>
          </div>
          <div className="tax-text-container">
            <p>
              Local taxes included (where applicable)
              <br />* Additional duties and taxes <sub>may apply</sub>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderHistoryCard;
