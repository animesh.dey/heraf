import React from "react";
import "./ReceiptModal.css";
import CloseIcon from "../../../assets/close.png";
import { Modal } from "react-bootstrap";
import DemoProductImg from "../../../assets/img_big.jpg";
import FullStar from "../../../assets/fullstar.png";
import HalfStar from "../../../assets/halfstar.png";
import RightArrow from "../../../assets/right-arrow.png";

const ReceiptModal = (props) => {
  const onHideHandler = () => {
    props.onHide();
  };
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.onHide}
        style={{ border: "none", outline: "none" }}
        dialogClassName="ReceiptModal"
      >
        <div className="ReceiptModal-body">
          <div className="closeIcon-container">
            <img src={CloseIcon} alt="" onClick={onHideHandler} />
          </div>
          <div className="heading">
            <h1>order #2116557899</h1>
            <p>
              Purchased from &nbsp; <sub>The Authentic Corner</sub> on November
              19,2021
            </p>
          </div>
          <div className="product-img-title">
            <div className="product-img">
              <img src={DemoProductImg} alt="" />
            </div>
            <div className="product-title">
              <p>
                Custom T-shirt, Mens Custom Shirt, Personalised T-shirts,
                Customised Apparel, Custom Tee • Rose Gold • NH02F66
              </p>
            </div>
          </div>
          <div className="review-container">
            <p className="first">
              Blade& #39;s Design and Leather Variations: Rare Viking Axe
            </p>
            <p className="second">
              Personalization: Not requested on this item.
            </p>
            <p className="transaction-id">
              Transaction: <span>#2537575747</span>
            </p>
            <p className="quan">Quantity: 1</p>
            <div className="rc">
              <div className="your-review-container common-review">
                <div className="review-text-container">
                  <p>Your Review</p>
                </div>
                <div className="review-star-container">
                  <img src={FullStar} alt="" />
                  <img src={FullStar} alt="" />
                  <img src={FullStar} alt="" />
                  <img src={FullStar} alt="" />
                  <img src={HalfStar} alt="" />
                </div>
              </div>
              <div className="edit-review-container common-review">
                <div className="review-text-container">
                  <p>Edit Review</p>
                </div>
                <div className="review-star-container">
                  <img src={RightArrow} alt="" className="rightArrow" />
                </div>
              </div>
            </div>
          </div>
          <div className="payment-method-details">
            <div className="payment-heading">
              <h3>Payment Method</h3>
            </div>
            <div className="payment-details-containers">
              <div className="left">
                <p>
                  Apple Pay
                  <br /> <br /> Paid on November 19, 2021 Your credit card
                  information was not shared with this shop.
                </p>
                <br />
                <p>Applied Discounts</p>
                <p>THANKSGIVING: 70% off</p>
              </div>
              <div className="right">
                <div className="amount">
                  <div className="amount-sub">
                    <p>Item(s) Total:</p>
                    <p className="money">US$ 37.00</p>
                  </div>
                  <div className="amount-sub">
                    <p>Shop Discount:</p>
                    <p className="money">-US$ 9.25</p>
                  </div>
                </div>
                <div className="amount">
                  <div className="amount-sub">
                    <p>Subtotal:</p>
                    <p className="money">US$ 27.75</p>
                  </div>
                  <div className="amount-sub">
                    <p>
                      Delivery(To<sub>India</sub>):
                    </p>
                    <p className="money">-US$ 9.25</p>
                  </div>
                </div>
                <div className="amount total">
                  <div className="amount-sub">
                    <p className="total-order-quan">Order Total (1 Item):</p>
                    <p className="money total-money">US$ 227.75</p>
                  </div>
                </div>
                <div className="tax-container">
                  <p>
                    Local taxes included (where applicable)
                    <br />* Additional duties and taxes <span>may apply</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="button-container">
            <p>
              This item was part of a USD 185.39 purchase from multiple shops on
              Nov 22, 2021
            </p>
            <div className="button-print-container">
              <button type="button" className="btn btn-secondary">
                Print
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default ReceiptModal;
