import React from "react";
import "./PrivacyModal.css";
import { Modal, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";

const PrivacyModal = (props) => {
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.onHide}
        dialogClassName="modal-90w custom"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <div className="t-modal-container">
          <div className="t-modal-heading">
            <h1>Privacy Settings</h1>
            <p>
              Heraf uses cookies and smillar technologies to give
              <br />
              you a better experience, enabling things like.
            </p>
          </div>
          <div className="paras">
            <p>
              basic site functions
              <br />
              ensuring secure,safe transaction
              <br />
              secure account login
              <br />
              remembering account, browser, and regional preferences
              <br />
              remembering privacy and security settings
              <br />
              analysing site traffic and usage
              <br />
              analysing site traffic and usage
              <br />
              analysing site traffic and usage
              <br />
              analysing site traffic and usage
              <br />
              Detailed information can be found in Herafs{" "}
              <Link to="" className="link">
                Cookies & Similar
              </Link>{" "}
              <br />
              <Link to="" className="link">
                Technologies Policy
              </Link>{" "}
              and our{" "}
              <Link to="" className="link">
                Privacy Policy.
              </Link>
            </p>
          </div>
          <div className="required-cookie-container">
            <h6>Required Cookies & Technologies</h6>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit
              corrupti minima porro libero, laboriosam illo ipsa aspernatur
              nulla reiciendis consectetur eaque omnis eligendi ea error ipsum.
              Maxime assumenda consectetur voluptates.
            </p>
          </div>
          <div className="personalized-container">
            <div className="header">
              <div className="h1">
                <h6>Personalised Advertising</h6>
              </div>
              <div className="form-container">
                <Form className="form">
                  <p>On</p> &nbsp;&nbsp;
                  <Form.Check
                    type="switch"
                    id="custom-switch"
                    className="switch"
                  />
                </Form>
              </div>
            </div>
            <div className="detail">
              <p>
                These are third party technologies used for things like interest
                based Heraf ads.
              </p>
              <p>
                We do this with marketing and advertising partners (who may have
                their <br /> own information they've collected) Saying no will
                not stop you from seeing Heraf <br /> ads or impact Heraf's own
                personalisation technologies. but it may make the <br /> ads you
                see less relevant or more repetitive. FInd out more in our{" "}
                <br />
                <span>Cookies & Similar Technologies Policy.</span>
              </p>
            </div>
          </div>
          <div className="button-container">
            <Button variant="secondary" className="button">
              Secondary
            </Button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default PrivacyModal;
