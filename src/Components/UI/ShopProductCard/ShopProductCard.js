import React from "react";
import "./ShopProductCard.css";
import { Card } from "react-bootstrap";
import HeartIcon from "../../../assets/heart-icon.png";

const ShopProductCard = (props) => {
  return (
    <Card className="s-Card" style={{ border: "hidden", outline: "none" }}>
      <div className="img-fav-container">
        <div className="img">
          <img src={props.img} alt="" />
        </div>
        <div className="fav">
          <img src={HeartIcon} alt="" />
        </div>
      </div>
      <div className="product-details">
        <p className="desc">{props.desc}</p>
        <h5>{props.price}</h5>
        {props.other !== "" && <p className="other">{props.other}</p>}
      </div>
    </Card>
  );
};

export default ShopProductCard;
