import React from "react";
import "./NavigationBarCom.css";
import { Navbar, Container, Nav } from "react-bootstrap";
import HerafLogo from "../../assets/logo.png";
import { Link } from "react-router-dom";
// import { BsArrowRight } from "react-icons/bs";
import Arrow from "../../assets/arrow_icon.png";
import { NavLink } from "react-router-dom";

const NavigationBarCom = () => {
  return (
    <div className="navigationBarCom">
      <Navbar expand="lg" className="navbar">
        <Container className="navbar-container">
          <Navbar.Brand>
            <Link to="">
              <img
                src={HerafLogo}
                width="70%"
                height="70%"
                className="d-inline-block align-top"
                alt="Heraf Logo"
              />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="first-nav">
              <NavLink to="/about" className="nav-link">
                About
              </NavLink>
              <NavLink to="/teams" className="nav-link">
                Teams
              </NavLink>
              <NavLink to="/careers" className="nav-link">
                Careers
              </NavLink>
              <NavLink to="/press" className="nav-link">
                Press
              </NavLink>
              <NavLink to="/investors" className="nav-link">
                Investors
              </NavLink>
              <NavLink to="/savedjobs" className="nav-link">
                Saved Jobs
              </NavLink>
            </Nav>
            <Nav className="second-nav">
              <Nav.Link className="nav-link" as={Link} to="">
                Stay Connected &nbsp;
                <img src={Arrow} alt="" className="bs-arrow-right" />
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavigationBarCom;
