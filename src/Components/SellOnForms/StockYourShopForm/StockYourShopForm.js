import React, { useState } from "react";
import "./StockYourShopForm.css";
import PlusIcon from "../../../assets/plus-icon.png";
import { Card } from "react-bootstrap";
import AddNewLstingForm from "../AddNewListingForm/AddNewLstingForm";

const demo = [
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
  { name: "ani" },
];

const StockYourShopForm = (props) => {
  const [showListingForm, setShowListingForm] = useState(false);

  const uploadImageHandler = () => {
    setShowListingForm(true);
    props.viewHeadingHandler(true);
    // document.getElementById("imguploader").click();
  };
  const cancelHandler = () => {
    setShowListingForm(false);
    props.viewHeadingHandler(false);
  };
  return (
    <>
      <div className="StockYourShopForm">
        {/* <input
          id="imguploader"
          type="file"
          multiple
          accept="image/*"
          onChange={(e) => setListingImage(e.target.value)}
        /> */}
        {!showListingForm && (
          <div className="img-isting-container container">
            <div className="row row-cols-5">
              {demo.map((item, index) => (
                <div className="col" key={index}>
                  <Card
                    style={{ border: "hidden", outline: "none" }}
                    className="card-container"
                  >
                    {/* { <Card.Img variant="top" src="" />} */}

                    {index < 1 && (
                      <div className="body">
                        <button onClick={uploadImageHandler}>
                          <img src={PlusIcon} alt="plusIcon" />
                        </button>
                      </div>
                    )}
                  </Card>
                </div>
              ))}
            </div>
          </div>
        )}

        {showListingForm && <AddNewLstingForm cancelHandler={cancelHandler} />}
      </div>
    </>
  );
};

export default StockYourShopForm;
