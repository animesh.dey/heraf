import React from "react";
import "./ShopPreferencesForm.css";
import { Form } from "react-bootstrap";

const ShopPreferencesForm = (props) => {
  return (
    <div className="ShopPreferencesForm">
      <div className="first-container">
        <div className="left">
          <div className="label-container">
            <p>
              Shop Container &nbsp;<sup>*</sup>
            </p>
            <p>
              Shop Country &nbsp;<sup>*</sup>
            </p>
            <p>
              Shop Currency &nbsp;<sup>*</sup>
            </p>
          </div>
          <div className="select-container">
            <Form.Select aria-label="Default select example" className="select">
              <option>Open this select menu</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
            <Form.Select aria-label="Default select example" className="select">
              <option>Open this select menu</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
            <Form.Select aria-label="Default select example" className="select">
              <option>Open this select menu</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
          </div>
        </div>
        <div className="right">
          <p>
            The default language you'll use to describe your items. Choose
            carefully! You can't change this once you save it, but may add other
            languages later.
          </p>

          <p>
            Tell us where your shop is based. Don’t see your country? We may not
            be available there right now, but stay tuned. Learn more
          </p>

          <p>
            The currency you'll use to price your items. Shoppers in other
            countries will automatically see prices in their local currency.
          </p>
        </div>
      </div>
      <div className="second-container">
        <div className="left">
          <div className="label-container">
            <p>
              Which of these best describes you?&nbsp; <sup>*</sup>
            </p>
          </div>
          <div className="radio-container">
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="flexRadioDefault"
                id="flexRadioDefault1"
              />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Selling is my full-time job
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="flexRadioDefault"
                id="flexRadioDefault2"
              />
              <label className="form-check-label" htmlFor="flexRadioDefault2">
                I sell part-time but hope to sell full-time
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="flexRadioDefault"
                id="flexRadioDefault3"
              />
              <label className="form-check-label" htmlFor="flexRadioDefault3">
                I sell part-time and that’s how I like it
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="flexRadioDefault"
                id="flexRadioDefault4"
              />
              <label className="form-check-label" htmlFor="flexRadioDefault4">
                Other
              </label>
            </div>
          </div>
        </div>
        <div className="right-single-para">
          <p>
            This is just an FYI for us and won’t affect the opening of your
            shop.
          </p>
        </div>
      </div>
    </div>
  );
};

export default ShopPreferencesForm;
