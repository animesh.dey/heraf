import React, { useState } from "react";
import "./AddNewListingForm.css";
import { Card, Form, InputGroup } from "react-bootstrap";
import OpenCloseArrow from "../../../assets/open-close-arrow.png";
import PrimaryIcon from "../../../assets/photo1-icon.png";
import EveryIcon from "../../../assets/photo2-icon.png";
import EveryIcon2 from "../../../assets/photo3-icon.png";
import EveryIcon3 from "../../../assets/photo4-icon.png";
import CommonIcon from "../../../assets/photo5-icon.png";
import CameraIcon from "../../../assets/white-camera-icon.png";
import VideoIcon from "../../../assets/white-video-icon.png";
import CrossIcon from "../../../assets/close.png";
import LightIcon from "../../../assets/light-icon.png";
import SearchIcon from "../../../assets/search.png";
import Arrow from "../../../assets/arrow_icon.png";

const AddNewLstingForm = (props) => {
  const [openClosePhoto, setOpenClosePhoto] = useState(false);
  const [openCloseVideos, setOpenCloseVideos] = useState(false);
  const [hideVideoStatement, setHideVideoStatement] = useState(false);
  const [openCloseListing, setOpenCloseListing] = useState(false);
  const [openCloseInventory, setOpenCloseInventory] = useState(false);
  const [openClosePersonalisation, setOpenClosePersonalisation] =
    useState(false);
  const [openCloseDigitalFiles, setOpenCloseIDigitalFiles] = useState(false);

  const openCloseHandler = () => {
    setOpenClosePhoto(!openClosePhoto);
  };
  const openCloseVideosHandler = () => {
    setOpenCloseVideos(!openCloseVideos);
  };
  const hideVideoStatementHandler = () => {
    setHideVideoStatement(true);
  };
  const openCloseListingHandler = () => {
    setOpenCloseListing(!openCloseListing);
  };
  const openCloseInventoryHandler = () => {
    setOpenCloseInventory(!openCloseInventory);
  };
  const openClosePersonalisationHandler = () => {
    setOpenClosePersonalisation(!openClosePersonalisation);
  };
  const openCloseDigitalFilesHandler = () => {
    setOpenCloseIDigitalFiles(!openCloseDigitalFiles);
  };
  return (
    <div className="AddNewLstingForm">
      <div className="photos-container ctn">
        <div className="label-container" onClick={openCloseHandler}>
          <p>Photos</p>
          {!openClosePhoto && (
            <img src={OpenCloseArrow} alt="" className="openarrow" />
          )}
          {openClosePhoto && (
            <img src={OpenCloseArrow} alt="" className="closearrow" />
          )}
        </div>
        {!openClosePhoto && (
          <div className="main-container">
            <div className="text-container">
              <p>Photos</p>
              <p>Add as many as you can so buyers can see every detail. </p>
              <br />
              <p>Add as many as you can so buyers can see every detail.</p>
              <p>Photos *</p>
              <p>
                Use up to ten photos to show your item's most important
                qualities
              </p>
              <p>Tips:</p>
              <p className="li">
                • Show the item being held, worn, or used.
              </p>{" "}
              <p className="li">• Show the item being held, worn, or used.</p>{" "}
              <p className="li">• Show the item being held, worn, or used.</p>{" "}
              <p className="li">• Shoot against a clean, simple background.</p>{" "}
              <p className="li">• Add photos to your variations.</p>
            </div>
            <div className="add-photo-container">
              <div className="container">
                <div className="row row-cols-5">
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="body">
                        <button>
                          <img src={CameraIcon} alt="CameraIcon" />
                          <p>Add a Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={PrimaryIcon} alt="PrimaryIcon" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={EveryIcon} alt="EveryIcon" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={EveryIcon2} alt="EveryIcon2" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={EveryIcon3} alt="EveryIcon3" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={CommonIcon} alt="CommonIcon" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={CommonIcon} alt="CommonIcon" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                  <div className="col">
                    <Card
                      style={{ border: "hidden", outline: "none" }}
                      className="card-container"
                    >
                      {/* { <Card.Img variant="top" src="" />} */}
                      <div className="others-body">
                        <button>
                          <img src={CommonIcon} alt="CommonIcon" />
                          <p>Primary Photo</p>
                        </button>
                      </div>
                    </Card>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      {/*videos-container */}
      <div className="videos-container ctn">
        <div className="label-container" onClick={openCloseVideosHandler}>
          <p>Videos</p>
          {!openCloseVideos && (
            <img src={OpenCloseArrow} alt="" className="openarrow" />
          )}
          {openCloseVideos && (
            <img src={OpenCloseArrow} alt="" className="closearrow" />
          )}
        </div>
        {!openCloseVideos && (
          <div className="main-container">
            <div className="text-container">
              <p>Videos</p>
              <p>
                Bring your product to life with a 5 to 15 second video - it
                could help you drive more sales. The video won’t feature sound,
                so let your product do the talking!
              </p>
              <p>Quick tips:</p>
              <p className="li">
                • Film wearable items on a model or show a functional item being
                used.
                <br />
                • Adjust your settings to record high resolution video - aim for
                1080p or higher.
                <br />• Crop your video after you upload it to get the right
                dimensions.
              </p>
            </div>
            <div className="add-photo-container">
              <div className="right-text-video">
                <div className="left">
                  <Card
                    style={{ border: "hidden", outline: "none" }}
                    className="card-container"
                  >
                    {/* { <Card.Img variant="top" src="" />} */}
                    <div className="body">
                      <button>
                        <img src={VideoIcon} alt="VideoIcon" />
                        <p>Add a Video</p>

                        <p style={{ fontSize: "10px" }}>Max file size 100MB</p>
                      </button>
                    </div>
                  </Card>
                </div>
                {!hideVideoStatement && (
                  <div className="right">
                    <div className="cross-icon-container">
                      <img
                        src={CrossIcon}
                        alt=""
                        onClick={hideVideoStatementHandler}
                      />
                    </div>
                    <div className="video-statement-card">
                      <div className="light-icon-container">
                        <img src={LightIcon} alt="" />
                      </div>
                      <div className="statement-containner">
                        <p>Buyers are loving listing videos!</p>

                        <p>
                          We know that shoppers are more likely to purchase an
                          item if the listing includes a video. Cha-ching!*
                        </p>

                        <p>
                          *Based on a July 2020 analysis of over 5 million
                          buyers, comparing the purchasing behaviour of those
                          who were shown listing videos to those who were not.
                        </p>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        )}
      </div>
      {/*Listing Container*/}
      <div className="listing-container ctn">
        <div className="label-container" onClick={openCloseListingHandler}>
          <p>Listing</p>
          {!openCloseListing && (
            <img src={OpenCloseArrow} alt="" className="openarrow" />
          )}
          {openCloseListing && (
            <img src={OpenCloseArrow} alt="" className="closearrow" />
          )}
        </div>
        {!openCloseListing && (
          <div className="main-listing-container">
            <div className="sub-container">
              <div className="left">
                <p>Listing Details</p>
                <p>
                  Tell the world all about your item and why they’ll love it.{" "}
                </p>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Title *</p>
                <p>
                  Include keywords that buyers would use to search for your
                  item.
                </p>
              </div>
              <div className="right">
                <InputGroup className="mb-3">
                  <Form.Control
                    aria-label=""
                    aria-describedby="basic-addon1"
                    className="title-input"
                  />
                </InputGroup>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>About this listing *</p>
                <p className="decoration-needed-p">
                  Learn more about what types of items are allowed on Heraf.
                </p>
              </div>
              <div className="right">
                <Form.Select aria-label="" className="made-it select">
                  <option>Who made it?</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </Form.Select>{" "}
                <Form.Select aria-label="" className="is-it select">
                  <option>What is it?</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </Form.Select>{" "}
                <Form.Select aria-label="" className="make-it select">
                  <option>When did you make it?</option>
                  <option value="1">One</option>
                  <option value="2">Two</option>
                  <option value="3">Three</option>
                </Form.Select>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Category *</p>
                <p>
                  Type a two- or three-word description of your item to get
                  category suggestions that will help more shoppers find it.
                </p>
              </div>
              <div className="right">
                <InputGroup className="mb-3">
                  <InputGroup.Text id="basic-addon1" className="btcn">
                    <button>
                      <img src={SearchIcon} alt="" />
                    </button>
                  </InputGroup.Text>
                  <Form.Control
                    placeholder="coats, earrings, hanging photo-frame"
                    aria-label=""
                    aria-describedby="basic-addon1"
                    className="category-search-input"
                  />
                </InputGroup>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Sub-Category *</p>
                <p>
                  Type a two- or three-word description of your item to get
                  sub-category suggestions that will help more shoppers find it.
                </p>
              </div>
              <div className="right">
                <InputGroup className="mb-3">
                  <InputGroup.Text id="basic-addon1" className="btcn">
                    <button>
                      <img src={SearchIcon} alt="" />
                    </button>
                  </InputGroup.Text>
                  <Form.Control
                    placeholder="coats, earrings, hanging photo-frame"
                    aria-label=""
                    aria-describedby="basic-addon1"
                    className="category-search-input"
                  />
                </InputGroup>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Renewal options *</p>
                <p>
                  Each renewal lasts for four months or until the listing sells
                  out.{" "}
                  <span className="decoration-needed-p">
                    Get more details on auto-renewing
                  </span>
                  <br />
                  <br />
                  Type *
                </p>
              </div>
              <div className="right-radio">
                <div className="radio-container">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault1"
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault1"
                    >
                      Automatic
                      <br />
                      The listng will renew as it expitres for $ 0.20 USD each
                      time (recommended).
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault2"
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault2"
                    >
                      Automatic
                      <br />
                      The listng will renew as it expitres for $ 0.20 USD each
                      time (recommended).
                    </label>
                  </div>
                </div>
                <div className="radio-container">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault3"
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault3"
                    >
                      Automatic
                      <br />
                      The listng will renew as it expitres for $ 0.20 USD each
                      time (recommended).
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="flexRadioDefault"
                      id="flexRadioDefault4"
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault4"
                    >
                      Automatic
                      <br />
                      The listng will renew as it expitres for $ 0.20 USD each
                      time (recommended).
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Description *</p>
                <p>
                  Each renewal lasts for four months or until the listing sells
                  out.{" "}
                  <span className="decoration-needed-p">
                    Get more details on auto-renewing
                  </span>
                </p>
              </div>
              <div className="right">
                <div className="text-area-container">
                  <InputGroup>
                    <Form.Control
                      as="textarea"
                      aria-label=""
                      className="text-area-input"
                    />
                  </InputGroup>
                  <p className="google-search-statement">
                    Preview listing as a Google search result{" "}
                    <sub>Show preview</sub>
                  </p>
                </div>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Productioon Partners</p>
                <p className="decoration-needed-p">Is this required for you?</p>

                <p>
                  A production partner is anyone who’s not a part of your Etsy
                  shop who helps you physically produce your items.
                </p>
              </div>
              <div className="right">
                <div className="btn-production-partner">
                  <p>Add a new production partner</p>
                  <button type="button">
                    <img src={Arrow} alt="" />
                  </button>
                </div>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Section (Optional)</p>
              </div>
              <div className="right">
                <div className="add-first-selection-container">
                  <p>
                    Group related listings into Sections to help shoppers browse
                    (e.g., Bracelets, Father’s Day Gifts, Yarn).
                  </p>
                  <p className="decoration-needed-r">Add your first section</p>
                </div>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>
                  What words might someone use to search for your listings? Use
                  all 13 tags to get found.{" "}
                  <span className="decoration-needed-p">
                    Get ideas for tags.
                  </span>
                </p>
              </div>
              <div className="right">
                <div className="tags-input-container">
                  <InputGroup className="mb-3">
                    <Form.Control
                      placeholder="share color, style, functions etc."
                      aria-label=""
                      aria-describedby="basic-addon2"
                      className="tag-input"
                    />
                    <InputGroup.Text id="basic-addon2">
                      <button type="button">Add</button>
                    </InputGroup.Text>
                  </InputGroup>
                  <p>13 left</p>
                </div>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Materials (Optional) </p>
              </div>
              <div className="right">
                <div className="tags-input-container">
                  <InputGroup className="mb-3">
                    <Form.Control
                      placeholder="ingredients, components etc."
                      aria-label=""
                      aria-describedby="basic-addon2"
                      className="tag-input"
                    />
                    <InputGroup.Text id="basic-addon2">
                      <button type="button">Add</button>
                    </InputGroup.Text>
                  </InputGroup>
                  <p>13 left</p>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      {/*Inventory and Pricing*/}
      <div className="inventory-container ctn">
        <div className="label-container" onClick={openCloseInventoryHandler}>
          <p>Inventory and Pricing</p>
          {!openCloseInventory && (
            <img src={OpenCloseArrow} alt="" className="openarrow" />
          )}
          {openCloseInventory && (
            <img src={OpenCloseArrow} alt="" className="closearrow" />
          )}
        </div>
        {!openCloseInventory && (
          <div className="main-inventory-pricing-container">
            <div className="sub-container">
              <div className="left">
                <p>Inventory and pricing</p>

                <p>
                  Tell the world all about your item and why they’ll love it.{" "}
                </p>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Price *</p>

                <p>
                  Remember to factor in the costs of materials, labour, and
                  other business expenses. If you offer free delivery, make sure
                  to include the cost of postage so it doesn’t eat into your
                  profits.
                </p>
              </div>
              <div className="right">
                <InputGroup className="mb-3">
                  <Form.Control
                    placeholder="$"
                    aria-label=""
                    aria-describedby="basic-addon1"
                    className="price-input"
                  />
                </InputGroup>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>Quantity*</p>

                <p>
                  For quantities greater than one, this listing will renew
                  automatically until it sells out. You’ll be charged a $ 0.20
                  USD listing fee each time.
                </p>
              </div>
              <div className="right">
                <InputGroup className="mb-3">
                  <Form.Control
                    placeholder="1"
                    aria-label=""
                    aria-describedby="basic-addon1"
                    className="price-input"
                  />
                </InputGroup>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>SKU (Optional)</p>

                <p>
                  For quantities greater than one, this listing will renew
                  automatically until it sells out. You’ll be charged a $ 0.20
                  USD listing fee each time.
                </p>
              </div>
              <div className="right">
                <InputGroup className="mb-3">
                  <Form.Control
                    placeholder="1"
                    aria-label=""
                    aria-describedby="basic-addon1"
                    className="price-input"
                  />
                </InputGroup>
              </div>
            </div>
          </div>
        )}
      </div>
      {/*Personalsation*/}
      <div className="personalisation-container ctn">
        <div
          className="label-container"
          onClick={openClosePersonalisationHandler}
        >
          <p>Personalisation</p>
          {!openClosePersonalisation && (
            <img src={OpenCloseArrow} alt="" className="openarrow" />
          )}
          {openClosePersonalisation && (
            <img src={OpenCloseArrow} alt="" className="closearrow" />
          )}
        </div>
        {!openClosePersonalisation && (
          <div className="main-Personalisation-container">
            <div className="sub-container">
              <div className="left">
                <p>Personalisation</p>
                <p>Collect personalised information for this listing.</p>
              </div>
              <div className="right">
                <p>Off</p>
                <div className="form-check form-switch">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    role="switch"
                    id="flexSwitchCheckDefault"
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      {/*Digital files*/}
      <div className="digitalFiles-container ctn">
        <div className="label-container" onClick={openCloseDigitalFilesHandler}>
          <p>Digital Files</p>
          {!openCloseDigitalFiles && (
            <img src={OpenCloseArrow} alt="" className="openarrow" />
          )}
          {openCloseDigitalFiles && (
            <img src={OpenCloseArrow} alt="" className="closearrow" />
          )}
        </div>
        {!openCloseDigitalFiles && (
          <div className="main-DigitalFiles-container">
            <div className="sub-container">
              <div className="left">
                <p>Digital files</p>
                <p>
                  Buyers can download these files as soon as they complete their
                  purchase.
                </p>
              </div>
              <div className="right">
                <p>
                  <span className="decoration-needed">Upload file</span>
                  <br />
                  Add up to 5 more files
                </p>
              </div>
            </div>
            <div className="sub-container">
              <div className="left">
                <p>
                  You can send a customised note to buyers of digital items
                  after the item is downloaded.
                </p>
              </div>
              <div className="right-policy">
                <p>
                  By adding files to this listing, you guarantee that you have
                  rights to the content. Etsy may remove content per our{" "}
                  <span className="decoration-needed">
                    Intellectual
                    <br /> Property Policy
                  </span>
                  , at which point buyers may not be able to access purchased
                  files. See our{" "}
                  <span className="decoration-needed">Terms</span> for more
                  information.
                </p>
              </div>
            </div>
          </div>
        )}
      </div>
      {/*button-container*/}
      <div className="buttons-last-container ctn">
        <div className="left-button-container">
          <p className="cancel" onClick={props.cancelHandler}>
            Cancel
          </p>
          <button className="left-button">
            <img src={Arrow} alt="" />
          </button>
          <p className="statement">
            This listing isn’t active yet. It will be available to shoppers once
            you open your shop.
          </p>
        </div>
        <div className="right-button-container">
          <button className="preview">Preview</button>
          <button className="saveandcontinue" onClick={props.cancelHandler}>
            Save and Continue <img src={Arrow} alt="" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default AddNewLstingForm;
