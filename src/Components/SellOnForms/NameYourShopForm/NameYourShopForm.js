import React from "react";
import "./NameOnYourShop.css";
import Arrow from "../../../assets/arrow_icon.png";
import { InputGroup, Form } from "react-bootstrap";

const NameYourShopForm = () => {
  return (
    <div className="NameYourShopForm">
      <Form className="search-container">
        <InputGroup className="mb-3">
          <Form.Control aria-label="" className="control" placeholder="Enter your shop name" />
          <InputGroup.Text>20</InputGroup.Text>
        </InputGroup>
        <div className="button-container">
          <p>Check Availability</p>
          <button type="submit" className="btn btn-secondary">
            <img src={Arrow} alt="" />
          </button>
        </div>
      </Form>

      <div className="text-container">
        <p>
          Your shop name will appear in your shop and next to each of your
          listings throughout Etsy.
          <br /> After you open your shop, you can change your name once.
        </p>
      </div>
    </div>
  );
};

export default NameYourShopForm;
