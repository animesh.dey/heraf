import React, { useState } from "react";
import "./SellOnForms.css";
import ShopPreferencesForm from "./ShopPreferencesForm/ShopPreferencesForm";
import NameYourShopForm from "./NameYourShopForm/NameYourShopForm";
import StockYourShopForm from "./StockYourShopForm/StockYourShopForm";
import SetUpBillingForm from "./SetUpBillingForm/SetUpBillingForm";
import Arrow from "../../assets/arrow_icon.png";
import { ProgressBar } from "react-bootstrap";

const SellOnForms = () => {
  const [page, setPage] = useState(0);
  const [ShowHeading, setShowHeading] = useState(false);
  const [formTitlesAndPara] = useState({
    titles: [
      "Shop Preferences",
      "Name Your Shop",
      "Stock Your Shop",
      "Set up billing",
    ],
    para: [
      "Let's get started! Tell us about you and your shop.",
      "Choose a memorable name that reflects your style.",
      "Add as many listings as you can. Ten or more would be a great start. More listings means more chances to be discovered!",
      "Demo Billing",
    ],
  });
  const viewHeadingHandler = (isView) => {
    setShowHeading(isView);
  };
  const displayForms = () => {
    if (page === 0) {
      return <ShopPreferencesForm />;
    } else if (page === 1) {
      return <NameYourShopForm />;
    } else if (page === 2) {
      return <StockYourShopForm viewHeadingHandler={viewHeadingHandler} />;
    } else if (page === 3) {
      return <SetUpBillingForm />;
    }
  };

  return (
    <div className="SellOnForms">
      <div className="forms-name-progresbar-container">
        <div className="forms-names">
          <p style={{ color: page >= 0 ? "#798273" : "#e0e2df" }}>
            Shop Preferences
          </p>
          <p style={{ color: page > 0 ? "#798273" : "#e0e2df" }}>
            Name Your Shop
          </p>
          <p style={{ color: page > 1 ? "#798273" : "#e0e2df" }}>
            Stock Your Shop
          </p>
          <p style={{ color: page === 3 ? "#798273" : "#e0e2df" }}>
            Set up billing
          </p>
        </div>
        <div className="progressbar-container">
          <ProgressBar
            now={
              page === 0
                ? "20"
                : page === 1
                ? "50"
                : page === 2
                ? "85"
                : page === 3
                ? "100"
                : null
            }
          />
        </div>
      </div>
      <div className="form-title-container">
        {!ShowHeading && <h1>{formTitlesAndPara.titles[page]}</h1>}
        {ShowHeading && <h1>Add a new listing</h1>}
        {!ShowHeading && <p>{formTitlesAndPara.para[page]}</p>}
      </div>

      <div className="main-form-container">{displayForms()}</div>

      {!ShowHeading && (
        <div className="button-container">
          <button
            disabled={page === 0}
            type="button"
            className="btn btn-secondary prev-btn"
            onClick={() => setPage((currPage) => currPage - 1)}
          >
            Previous
          </button>
          <button
            disabled={page === formTitlesAndPara.titles.length - 1}
            type="button"
            className="btn btn-secondary save-btn"
            onClick={() => setPage((currPage) => currPage + 1)}
          >
            Save and Continue <img src={Arrow} alt="" />
          </button>
        </div>
      )}

      <div className="footer">
        <p>FAQs | Terms & Conditions | Privacy Policy | Help</p>
        <p>Copyright © 2021-22 © 2021 Heraf, Inc. | All Right Reserved.</p>
      </div>
    </div>
  );
};

export default SellOnForms;
