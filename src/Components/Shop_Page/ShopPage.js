import React, { useState } from "react";
import "./ShopPage.css";
import HeartIcon from "../../assets/heart-icon.png";
import ShopIcon from "../../assets/shop-logo.jpg";
import FullStar from "../../assets/fullstar.png";
import HalfStar from "../../assets/halfstar.png";
import StarIcon from "../../assets/star-icon.png";
import ContactIcon from "../../assets/contact-icon.png";
import DemoUserIcon from "../../assets/review-user1.jpg";
import { Form, InputGroup, FormControl, Button } from "react-bootstrap";
import SearchIcon from "../../assets/search.png";
import DemoShopProductIcon from "../../assets/shop_Product.jpg";
import ShopProductCard from "../UI/ShopProductCard/ShopProductCard";
import { NavLink } from "react-router-dom";
import DemoReviewImg from "../../assets/review1.jpg";
import ReviewDetailCard from "../UI/ReviewDetailCard/ReviewDetailCard";
import parse from "html-react-parser";
import LockRightIcon from "../../assets/lock-right.png";
import PolicyPaymentCard from "../UI/PolicyPaymentCard/PolicyPaymentCard";
import Paypal from "../../assets/paypal-logo-preview.png";
import Visa from "../../assets/visa.svg";
import MasterCard from "../../assets/payment3.svg";
import Maestro from "../../assets/Maestro.png";
import Discover from "../../assets/discover.svg";
import Payment6 from "../../assets/payment6.svg";

const demoOptions = [
  {
    option: "All",
    quan: 411,
  },
  {
    option: "On sale",
    quan: 1,
  },
  {
    option: "Single Beaded Bracelets",
    quan: 21,
  },
  {
    option: "Beaded Bracelet Stacks",
    quan: 50,
  },
  {
    option: "Made To Order Bracelets",
    quan: 75,
  },
  {
    option: "Seed Bead Necklaces",
    quan: 16,
  },
  {
    option: "Handmade Envelopes",
    quan: 11,
  },
  {
    option: "Printable Greeting Cards",
    quan: 25,
  },
  {
    option: "Printable  Wall Art",
    quan: 30,
  },
  {
    option: "Fun Paper Products",
    quan: 31,
  },
  {
    option: "Request Custom Order",
    quan: 4,
  },
  {
    option: "Contact shop owner",
    quan: 15,
  },
];

const demoShopProductDetails = [
  {
    pImg: DemoShopProductIcon,
    desc: "Seed Bead Braceletes, Bahemian Beaded..",
    price: "US$ 50",
    // other: "Over 20 people have this in their basket",
  },
  {
    pImg: DemoShopProductIcon,
    desc: "Seed Bead Braceletes, Bahemian Beaded..",
    price: "US$ 50",
    other: "Over 20 people have this in their basket",
  },
  {
    pImg: DemoShopProductIcon,
    desc: "Seed Bead Braceletes, Bahemian Beaded..",
    price: "US$ 50",
    // other: "Over 20 people have this in their basket",
  },
  {
    pImg: DemoShopProductIcon,
    desc: "Seed Bead Braceletes, Bahemian Beaded..",
    price: "US$ 50",
    other: "Over 20 people have this in their basket",
  },
  {
    pImg: DemoShopProductIcon,
    desc: "Seed Bead Braceletes, Bahemian Beaded..",
    price: "US$ 50",
    other: "Over 20 people have this in their basket",
  },
  {
    pImg: DemoShopProductIcon,
    desc: "Seed Bead Braceletes, Bahemian Beaded..",
    price: "US$ 50",
    other: "Over 20 people have this in their basket",
  },
];

const demoReviewDetails = [
  {
    profileIcon: DemoUserIcon,
    profileName: "Animesh Dey",
    reviewDate: "20 sep,2021",
    FullStar,
    HalfStar,
    finish: "STERLING SILVER",
    length: "10 INCHES",
    review:
      "This necklace is absolutely beautiful, packaging is amazing and customer service is outstanding! I bought this necklace with my newborn daughter’s name and I honestly could not be any happier. Seller was quick with responses and sent me the nam …",
    reviewImg: DemoReviewImg,
  },
  {
    profileIcon: DemoUserIcon,
    profileName: "Animesh Dey",
    reviewDate: "20 sep,2021",
    FullStar,
    HalfStar,
    finish: "STERLING SILVER",
    length: "10 INCHES",
    review:
      "This necklace is absolutely beautiful, packaging is amazing and customer service is outstanding! I bought this necklace with my newborn daughter’s name and I honestly could not be any happier. Seller was quick with responses and sent me the nam …",
    reviewImg: DemoReviewImg,
  },
  {
    profileIcon: DemoUserIcon,
    profileName: "Animesh Dey",
    reviewDate: "20 sep,2021",
    FullStar,
    HalfStar,
    finish: "STERLING SILVER",
    length: "10 INCHES",
    review:
      "This necklace is absolutely beautiful, packaging is amazing and customer service is outstanding! I bought this necklace with my newborn daughter’s name and I honestly could not be any happier. Seller was quick with responses and sent me the nam …",
    reviewImg: DemoReviewImg,
  },
];

const ShopPage = () => {
  const [deliveryPolicy] = useState({
    heading: "Delivery",
    data: [
      {
        title: "Processing time",
        desc: "The time i need to prepare on order for dispatch varies. For details,see individual items",
      },
      {
        title: "Estimated delivery times",
        desc: parse(
          "North America: 1-2 business days <br /> I'll do my best to meet these dispatch estimates, but can't guarantee them, Actual delivery time will depend on the delivery method you choose."
        ),
      },
      {
        title: "Customs and import taxes",
        desc: "Buyers are responsible for any customs and import taxes that may apply. I'm not responsible for delays due to customs.",
      },
    ],
  });
  const [digitalDownloadPolicy] = useState({
    heading: "Digital Downloads",
    data: [
      {
        title: "File Delivery",
        desc: parse(
          "Your files will be available to download once payment is confirmed.<sub>Here's how.</sub>"
        ),
      },
    ],
  });
  const [paymentDownloadsPolicy] = useState({
    heading: "Payment Download",
    title: "Secure Options",
    desc: "Heraf keeps your payment information secure. Heraf shops never receive your credit information.",
  });
  const [PaymentPolicyMethodsImgs] = useState([
    Paypal,
    Visa,
    MasterCard,
    Maestro,
    Discover,
    Payment6,
  ]);
  const [returnPolicy] = useState({
    heading: "Returns & Exchanges",
    data: [
      {
        title: "I gladly accept exchanges",
        desc: parse(
          "Contact me within: 5 days of delivery<br />Dispatch items back within:14 days of delivery"
        ),
      },
      {
        title: "I don't accept returns or cancellations",
        desc: "But please contact me if you have any problems with your order",
      },
      {
        title: "The following items can't be returned or exchanged",
        desc: parse(
          "Because of the nature of these items,unless they arrive damaged or defective, i can't accept returns for:<ul><li>Custom or persoonalised orders</li><li>Digital downloads</li><li>Items on sale</li></ul>"
        ),
      },
      {
        title: "Conditions of return",
        desc: "Buyers are responsible for return postage costs. If the item is not returned in its original condition, the buyer is responsible for any loss in value",
      },
    ],
  });
  return (
    <div className="ShopPage">
      <div className="first-container">
        <div className="upper">
          <h1>The Authentic Corner</h1>
        </div>
        <div className="lower">
          <div className="card-container">
            <div className="logo">
              <img src={HeartIcon} alt="" />
            </div>
            <div className="text">
              <p>Favourite Shop (4.4k)</p>
            </div>
          </div>
        </div>
      </div>
      <div className="second-container">
        <div className="left-container">
          <div className="icon">
            <img src={ShopIcon} alt="" />
          </div>
          <div className="text-container">
            <h6>The Authentic Corner</h6>
            <p>Modern Accessories for Men & Women..</p>
            <h3>
              18,176
              <sub>
                <img src={FullStar} alt="" />
                <img src={FullStar} alt="" />
                <img src={FullStar} alt="" />
                <img src={FullStar} alt="" />
                <img src={HalfStar} alt="" />
              </sub>
            </h3>
            <p>
              <span className="star-icon">
                <img src={StarIcon} alt="" />
              </span>{" "}
              <span className="star-seller">Star Seller</span> Washington United
              States
            </p>
          </div>
        </div>
        <div className="middle-container">
          <div className="icon">
            <img src={StarIcon} alt="" />
          </div>
          <div className="text">
            <p>
              <span>Star Seller , </span> <br />
              This seller sets a shining example for providing a great customer
              experience, with a history of 5-star reviews, on-time dispatch,
              and quick replies to any message they received.
            </p>
          </div>
        </div>
        <div className="right-container">
          <div className="img-container">
            <img src={DemoUserIcon} alt="" />
          </div>
          <div className="details">
            <h6>The Authentic Corner</h6>
            <p>
              <span>
                <img src={ContactIcon} alt="" />
              </span>{" "}
              Contact
            </p>
          </div>
        </div>
      </div>
      <div className="third-container">
        <div className="announcement-container">
          <h5>Announcement</h5>
          <p>Last updated on 23 Mar,2021</p>
        </div>
        <div className="welcome-statement-container">
          <p>
            Welcome to Stones + Paper. The perfect spot for seed bead bracelets,
            printable cards and fun paper goods.
          </p>
        </div>
        <div className="sort-container">
          <p>Sortby: &nbsp;</p>
          <div className="select-input-field">
            <Form.Select
              aria-label="Default select example"
              className="select-field"
            >
              <option value="most-recent">Most Recent</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
          </div>
        </div>
      </div>
      <div className="fourth-container">
        <div className="items-filter-option-container">
          <div className="heading">
            <h5>Items</h5>
          </div>
          <div className="search-container">
            <div className="search-input">
              <Form className="form">
                <InputGroup>
                  <FormControl placeholder="all" className="control" required />
                  <InputGroup.Text>
                    <Button type="submit" className="button">
                      <img src={SearchIcon} alt="" />
                    </Button>
                  </InputGroup.Text>
                </InputGroup>
              </Form>
            </div>
          </div>
          <div className="filter-options-container">
            {demoOptions?.map((item, index) => (
              <div className="form-check form-check-inline" key={index}>
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="inlineCheckbox1"
                  value="option1"
                />
                <label className="form-check-label" htmlFor="inlineCheckbox2">
                  {item.option} <span>({item.quan})</span>
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="shop-product-listing container">
          <div className="row">
            {demoShopProductDetails.map((item, index) => (
              <div className="col-lg-4 col-md-6" key={index}>
                <ShopProductCard
                  img={item.pImg}
                  desc={item.desc}
                  price={item.price}
                  other={item.other}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="pagination-container fifth-container">
        <nav aria-label="Page navigation example">
          <ul className="pagination">
            <li className="page-item">
              <NavLink to="" className="page-link">
                Page
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                1
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                2
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                3
              </NavLink>
            </li>
            <li className="page-item">
              <NavLink to="" className="page-link">
                4
              </NavLink>
            </li>
            <li className="page-item next-link">
              <NavLink to="" className="page-link">
                Next
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
      <div className="sixth-container">
        <div className="review">
          <h6>Reviews</h6>
        </div>
        <div className="middle-container">
          <p>
            Average Item Reviews <img src={FullStar} alt="" />{" "}
            <img src={FullStar} alt="" /> <img src={FullStar} alt="" />{" "}
            <img src={FullStar} alt="" /> <img src={HalfStar} alt="" />{" "}
            <span>(9,670)</span>
          </p>
        </div>
        <div className="sort-container">
          <p>Sortby: &nbsp;</p>
          <div className="select-input-field">
            <Form.Select
              aria-label="Default select example"
              className="select-field"
            >
              <option value="most-recent">Most Recent</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </Form.Select>
          </div>
        </div>
      </div>
      <div className="seventh-container">
        {demoReviewDetails?.map((item, index) => (
          <ReviewDetailCard
            key={index}
            profileIcon={item.profileIcon}
            profileName={item.profileName}
            reviewDate={item.reviewDate}
            FullStar={item.FullStar}
            HalfStar={item.HalfStar}
            finish={item.finish}
            length={item.length}
            review={item.review}
            reviewImg={item.reviewImg}
          />
        ))}
        <div className="pagination-container">
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li className="page-item">
                <NavLink to="" className="page-link">
                  Page
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  1
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  2
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  3
                </NavLink>
              </li>
              <li className="page-item">
                <NavLink to="" className="page-link">
                  4
                </NavLink>
              </li>
              <li className="page-item next-link">
                <NavLink to="" className="page-link">
                  Next
                </NavLink>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div className="last-container">
        <div className="ready-wraper">
          <div className="heading">
            <h1>Shop Policy</h1>
            <p>Last updated on 01 Apr,2021</p>
          </div>
          <div className="policys-container">
            <div className="policy-container">
              <div className="left">
                <p>{deliveryPolicy.heading}</p>
              </div>
              <div className="right">
                {deliveryPolicy.data?.map((item, index) => (
                  <div className="policy-details" key={index}>
                    <h6>{item.title}</h6>
                    <p>{item.desc}</p>
                  </div>
                ))}
              </div>
            </div>
            <div className="policy-container">
              <div className="left">
                <p>{digitalDownloadPolicy.heading}</p>
              </div>
              <div className="right">
                {digitalDownloadPolicy.data?.map((item, index) => (
                  <div className="policy-details" key={index}>
                    <h6>{item.title}</h6>
                    <p>{item.desc}</p>
                  </div>
                ))}
              </div>
            </div>
            <div className="policy-container">
              <div className="left">
                <p>{paymentDownloadsPolicy.heading}</p>
              </div>
              <div className="right">
                <div className="policy-details">
                  <h6 className="h6">
                    <img
                      src={LockRightIcon}
                      alt=""
                      className="lock-right-icon"
                    />
                    &nbsp;
                    {paymentDownloadsPolicy.title}
                  </h6>
                  <div className="policy-payment-cards-container">
                    <div className=" container">
                      <div className="shop-product-listing">
                        <div className="row">
                          {PaymentPolicyMethodsImgs?.map((item, index) => (
                            <div
                              className="col-lg-4 col-md-6 img-padding"
                              key={index}
                            >
                              <PolicyPaymentCard img={item} />
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                  <p>{paymentDownloadsPolicy.desc}</p>
                </div>
              </div>
            </div>
            <div className="policy-container">
              <div className="left">
                <p>{returnPolicy.heading}</p>
              </div>
              <div className="right">
                {returnPolicy.data?.map((item, index) => (
                  <div className="policy-details" key={index}>
                    <h6>{item.title}</h6>
                    <div className="returnpolicy-desc">{item.desc}</div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopPage;
