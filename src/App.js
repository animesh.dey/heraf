import React from "react";
import OrderHistoryPage from "./Components/OrderHistoryPage/OrderHistoryPage";
import ShopPage from "./Components/Shop_Page/ShopPage";
import { Route, Routes } from "react-router-dom";
import AboutPage from "./Components/About_Page/AboutPage";
import CareerPage from "./Components/Careers_Page/CareerPage";
import NavigationBarCom from "./Components/NavigationBarCom/NavigationBarCom";
import ContactUsPage from "./Components/ContactUs_Page/ContactUsPage";
import DeliveryPage from "./Components/DeliveryAddress_Page/DeliveryPage";
import PaymentCardPage from "./Components/PaymentCard_Page/PaymentCardPage";
import PaymentPage from "./Components/Payment_Page/PaymentPage";
import TeamsPage from "./Components/Teams_Page/TeamsPage";
import PressPage from "./Components/Press_Page/PressPage";
import InvestorsPage from "./Components/Investors_Page/InvestorsPage";
import SavedJobsPage from "./Components/SavedJobs_Page/SavedJobsPage";
import HelpPage from "./Components/Help_Page/HelpPage";
import ShoppingListing from "./Components/ShoppingListing_Page/ShoppingListing";
import UserProfilePage from "./Components/UserProfile_Page/UserProfilePage";
import SellOnForms from "./Components/SellOnForms/SellOnForms";

const App = () => {
  return (
    <div className="App">
      {/* <PaymentPage /> */}
      {/* <DeliveryPage /> */}
      {/* <PaymentCardPage /> */}
      {/* <ContactUsPage /> */}
      {/* <NavigationBarCom /> */}
      {/* <AboutPage /> */}
      {/* <Routes>
        <Route path="/about" element={<AboutPage />} />
        <Route path="/careers" element={<CareerPage />} />
        <Route path="/teams" element={<TeamsPage />} />
        <Route path="/investors" element={<InvestorsPage />} />
        <Route path="/savedjobs" element={<SavedJobsPage />} />
        <Route path="/" element={<SellOnForms />} />
      </Routes> */}
      {/* <InvestorsPage /> */}
      {/* <SavedJobsPage /> */}
      {/* <TeamsPage /> */}
      {/* <PressPage /> */}
      {/* <HelpPage /> */}
      {/* <ShoppingListing /> */}
      {/* <ShopPage /> */}
      {/* <UserProfilePage /> */}
      {/* <OrderHistoryPage /> */}
      <SellOnForms />
    </div>
  );
};

export default App;
